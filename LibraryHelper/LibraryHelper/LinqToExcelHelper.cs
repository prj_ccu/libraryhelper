﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LinqToExcel;

namespace LibraryHelper
{
    public class LinqToExcelHelper
    {
        public string FileName;

        public string WorkSheetName;

        public IQueryable<Row> GetWorkSheetData()
        {
            var Excel = new ExcelQueryFactory(FileName);

            var Qry = from Item in Excel.Worksheet(WorkSheetName)
                      select Item;

            return Qry;
        }
    }
}