﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;

namespace LibraryHelper
{
    public class NPOIHelper
    {
        public Stream RenderDataTableToExcel(DataTable SourceTable)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream ms = new MemoryStream();
            HSSFSheet sheet = workbook.CreateSheet();
            HSSFRow headerRow = sheet.CreateRow(0);

            // handling header.
            foreach (DataColumn column in SourceTable.Columns)
                headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);

            // handling value.
            int rowIndex = 1;

            foreach (DataRow row in SourceTable.Rows)
            {
                HSSFRow dataRow = sheet.CreateRow(rowIndex);

                foreach (DataColumn column in SourceTable.Columns)
                {
                    dataRow.CreateCell(column.Ordinal).SetCellValue(row[column].ToString());
                }

                rowIndex++;
            }

            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;

            sheet = null;
            headerRow = null;
            workbook = null;

            return ms;
        }

        public DataTable RenderDataTableFromExcel(string FileName, int HeaderRowIndex)
        {
            HSSFWorkbook workbook;

            FileStream file;

            using (file = new FileStream(FileName, FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(file);
            }

            HSSFSheet sheet = workbook.GetSheetAt(0);

            DataTable table = new DataTable();

            HSSFRow headerRow = sheet.GetRow(HeaderRowIndex);

            //int cellCount = headerRow.LastCellNum;

            int cellCount = 7;

            try
            {
                for (int i = 0; i < cellCount; i++)
                {
                    DataColumn column = new DataColumn(headerRow.GetCell(i).ToString().Trim());
                    table.Columns.Add(column);
                }

                int rowCount = sheet.LastRowNum;

                for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                {
                    HSSFRow row = sheet.GetRow(i);
                    DataRow dataRow = table.NewRow();

                    for (int j = 0; j < cellCount; j++)
                    {
                        if (row.GetCell(j) != null)
                            dataRow[j] = row.GetCell(j).ToString().Trim();
                        else
                            dataRow[j] = "";
                    }

                    //dataRow[0] = row.GetCell(0).ToString().Trim();
                    //dataRow[1] = row.GetCell(1).ToString().Trim();
                    //dataRow[2] = row.GetCell(5).ToString().Trim();
                    //dataRow[3] = row.GetCell(6).ToString().Trim();

                    table.Rows.Add(dataRow);
                }
            }
            catch
            {
                //HttpContext.Current.Response.Write("Error! " + FileName + "<br>");
            }

            file.Close();
            workbook = null;
            sheet = null;
            return table;
        }

        public void Export(MemoryStream MS, string FileName)
        {
            //HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment; filename=" + FileName));

            //HttpContext.Current.Response.BinaryWrite(MS.ToArray());

            MS.Close();
            MS.Dispose();
        }
    }
}