﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;

namespace LibraryHelper
{
    public class NetworkHelper
    {
        public string IP;

        public Boolean Status;

        public void Ping()
        {
            Ping objPing = new Ping();

            PingReply PR = objPing.Send(IP);

            Status = PR.Status == IPStatus.Success ? true : false;

            //objPing.SendAsync(IP, null);

            //objPing.PingCompleted += new PingCompletedEventHandler(objPing_PingCompleted);
        }

        //private void objPing_PingCompleted(object sender, PingCompletedEventArgs e)
        //{
        //    Status = e.Reply.Status == IPStatus.Success ? true : false;
        //}
    }
}