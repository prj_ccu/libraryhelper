﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LibraryHelper
{
    public class SQLHelper
    {
        public void Add(string SQL, string ConnString)
        {
            using (SqlConnection Conn = new SqlConnection(ConnString))
            {
                Conn.Open();

                using (SqlCommand SqlCmd = new SqlCommand(SQL, Conn))
                    SqlCmd.ExecuteNonQuery();
            }
        }
    }
}