﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using OfficeOpenXml;

namespace LibraryHelper
{
    public class EPPlusHelper
    {
        public string FileName;

        public string WorkSheetName;

        public ExcelWorksheet GetWorkSheetData()
        {
            FileInfo _FileName = new FileInfo(FileName);

            using (ExcelPackage package = new ExcelPackage(_FileName))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.First();

                return worksheet;
            }
        }
    }
}