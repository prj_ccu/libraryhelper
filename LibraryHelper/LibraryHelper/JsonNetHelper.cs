﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LibraryHelper
{
    public class JsonNetHelper
    {
        public string DataTableToJson(DataTable DT)
        {
            return JsonConvert.SerializeObject(DT, Formatting.Indented);
        }

        public DataTable JsonToDataTable(string JsonData)
        {
            return JsonConvert.DeserializeObject<DataTable>(JsonData);
        }
    }
}