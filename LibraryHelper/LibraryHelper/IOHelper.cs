﻿using System.IO;

namespace LibraryHelper
{
    public class IOHelper
    {
        public void FolderCreate(string Path)
        {
            if (!(Directory.Exists(Path)))
            {
                Directory.CreateDirectory(Path);
            }
        }

        public int FolderFileCount(string Path)
        {
            return new DirectoryInfo(Path).GetFiles().Length;
        }

        public void FolderDelete(string Path)
        {
            if (Directory.Exists(Path))
            {
            }
        }
    }
}